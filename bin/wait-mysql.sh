#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.config.sh"

###################################
## MySQL
###################################
if [[ -n "$(dockerContainerId mysql)" ]]; then
    HAS_MYSQL=1
    while [ $HAS_MYSQL != 0 ]
    do
        sleep 1
        logMsg "Waiting for MySQL to be ready ..."
        MYSQL_ROOT_PASSWORD=$(dockerExecMySQL printenv MYSQL_ROOT_PASSWORD)
        dockerExecMySQL sh -c "MYSQL_PWD=\"${MYSQL_ROOT_PASSWORD}\" mysqladmin --silent -h mysql -uroot status"
        HAS_MYSQL=$?
    done
else
    echo " * Skipping mysql wait, no such container"
fi
