#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

if [ "$#" -lt 1 ]; then
    echo "No packagename has been specified"
    exit 1
fi

NEW_SITE_PACKAGE=$1
SIMPLE_NAME=${NEW_SITE_PACKAGE:8}
SIMPLE_NAME=$(echo "$SIMPLE_NAME" | tr '[:upper:]' '[:lower:]')
echo "New site package: $NEW_SITE_PACKAGE"
echo "Simple name: ${SIMPLE_NAME}"

LC_CTYPE=C
LANG=C

find app/Repository/Onedrop.Customername -type f -print0 | xargs -0 sed -i '' -e "s/Onedrop\.Customername/$NEW_SITE_PACKAGE/g"
find app/Repository/Onedrop.Customername -type f -print0 | xargs -0 sed -i '' -e "s/customername/$SIMPLE_NAME/g"

mv app/Repository/Onedrop.Customername app/Repository/${NEW_SITE_PACKAGE}
sed -i '' -e "s#onedrop/customername#onedrop/$SIMPLE_NAME#g" app/composer.json
