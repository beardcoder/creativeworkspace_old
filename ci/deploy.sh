#!/usr/bin/env bash

export FLOW_CONTEXT=Production

cd $DEPLOY_DIR

$PHP_BIN /usr/local/bin/composer install --no-dev --no-progress --no-suggest -ano
$PHP_BIN flow flow:cache:flush --force
$PHP_BIN flow flow:package:rescan
$PHP_BIN flow doctrine:migrate
$PHP_BIN flow resource:publish
