<?php
namespace BeardCoder\Creativeworkspace\ViewHelpers;

/*
 * This file is part of the DL.Gallery package.
 *
 * (c) Daniel Lienert 2016
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Neos\Media\Domain\Model\ImageInterface;

/**
 * Class ImageViewHelper
 * @scope Frontend
 */
class ImageViewHelper extends \Neos\Media\ViewHelpers\ImageViewHelper
{
    /**
     * Renders an HTML img tag with a thumbnail image, created from a given image.
     *
     * @param ImageInterface $image          The image to be rendered as an image
     * @param int            $width          Desired width of the image
     * @param int            $maximumWidth   Desired maximum width of the image
     * @param int            $height         Desired height of the image
     * @param int            $maximumHeight  Desired maximum height of the image
     * @param bool           $allowCropping  Whether the image should be cropped if the given sizes would hurt the aspect ratio
     * @param bool           $allowUpScaling Whether the resulting image size might exceed the size of the original image
     * @param bool           $async          Return asynchronous image URI in case the requested image does not exist already
     * @param string         $preset         Preset used to determine image configuration
     * @param int            $quality        Quality of the image
     *
     * @return string an <img...> html tag
     */
    public function render(
        ImageInterface $image = null,
        $width = null,
        $maximumWidth = null,
        $height = null,
        $maximumHeight = null,
        $allowCropping = false,
        $allowUpScaling = false,
        $async = false,
        $preset = null,
        $quality = null
    ) {
        if ($image === null) {
            return '';
        }

        if ($preset) {
            $thumbnailConfiguration = $this->thumbnailService->getThumbnailConfigurationForPreset($preset, $async);
        } else {
            $thumbnailConfiguration = new \Neos\Media\Domain\Model\ThumbnailConfiguration(
                $width,
                $maximumWidth,
                $height,
                $maximumHeight,
                $allowCropping,
                $allowUpScaling,
                $async
            );
        }
        $thumbnailData = $this->assetService->getThumbnailUriAndSizeForAsset(
            $image,
            $thumbnailConfiguration,
            $this->controllerContext->getRequest()
        );

        if ($thumbnailData === null) {
            return '';
        }

        $this->tag->addAttribute('data-src', $thumbnailData['src']);
        $this->tag->addAttribute(
            'src',
            'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
        );

        if ($thumbnailData['width'] > 0 && $thumbnailData['height'] > 0) {
            $this->tag->addAttributes([
                'width'  => $thumbnailData['width'],
                'height' => $thumbnailData['height'],
            ]);
        }

        // alt argument must be set because it is required (see $this->initializeArguments())
        if ($this->arguments['alt'] === '') {
            // has to be added explicitly because empty strings won't be added as attributes in general (see parent::initialize())
            $this->tag->addAttribute('alt', '');
        }

        return $this->tag->render();
    }
}
