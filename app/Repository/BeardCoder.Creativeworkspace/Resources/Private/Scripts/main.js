import LazyLoad from 'vanilla-lazyload';
import Feather from 'feather-icons';
import Gallery from './Module/Gallery';
import Webfontloader from 'webfontloader';
import Prism from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-markup-templating';
import 'prismjs/components/prism-php';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-yaml';
import 'prismjs/components/prism-markup';
import './Helper/Prism.typoscript';
import HeaderAffix from './Module/HeaderAffix';
import ProgressBar from './Module/ProgressBar';
import Minigrid from 'minigrid';

(() => {
    Feather.replace();
    Prism.highlightAll();
    HeaderAffix.init();
    ProgressBar.init();
    new Gallery('.gallery');
    Webfontloader.load({
        google: {
            families: ['PT Sans']
        }
    });

    window.myLazyLoad = new LazyLoad({
        data_src: 'src',
        data_srcset: 'srcset',
        elements_selector: '.lazy',
        callback_load: function () {
            if (window.grid !== undefined)
                window.grid.mount();
        }
    });

    if (document.getElementsByClassName('gallery').length >= 1) {
        let grid;
        const initMinigrid = function () {
            grid = window.grid = new Minigrid({
                container: '.gallery',
                item: '.gallery__image',
                gutter: 0
            });
            grid.mount();
            if (window.myLazyLoad !== undefined)
                window.myLazyLoad.update();
        };

        const update = function () {
            grid.mount();
            if (window.myLazyLoad !== undefined)
                window.myLazyLoad.update();
        };

        initMinigrid();

        if (window.myLazyLoad !== undefined) {
            setTimeout(function () {
                grid.mount();
            }, 200); // check again in a second
        }
        window.addEventListener('resize', update);
    }

    const navigation = {
        trigger: document.querySelector('.navigation-trigger'),
        class: 'navigation--open',
    };
    const body = document.querySelector('body');
    const addClassForNavigation = function () {
        body.classList.add(navigation.class);
        window.addEventListener('keydown', eventHandlerRemoveClassFromNavigation, false);
    };
    const removeClassForNavigation = function () {
        body.classList.remove(navigation.class);
    };

    const eventHandlerRemoveClassFromNavigation = function (eve) {
        if (eve.key === 'Escape') {
            body.classList.remove(navigation.class);
            window.removeEventListener('keydown', eventHandlerRemoveClassFromNavigation);
        }
    };

    ['touchend', 'click'].forEach(function (eventAction) {
        navigation.trigger.addEventListener(eventAction, function (e) {
            e.preventDefault();
            if (body.classList.contains(navigation.class)) {
                removeClassForNavigation();
            } else {
                addClassForNavigation();
            }
        });
    });
})();
