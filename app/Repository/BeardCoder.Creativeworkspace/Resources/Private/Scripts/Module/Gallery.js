import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI_Default from 'photoswipe/src/js/ui/photoswipe-ui-default';
import forEach from 'lodash/forEach';

export default class Gallery {
    /**
     * Initialize funtion
     */
    constructor(selector) {
        const galleryElements = document.querySelectorAll(selector);
        let index = 0;

        forEach(galleryElements, element => {
            element.setAttribute('data-pswp-uid', String(index + 1));
            element.onclick = this.onThumbnailsClick;
            element.Gallery = this;
            index++;
        });
    }

    /**
     * Parse thumbnail elements
     *
     * @param el
     * @returns {any[]}
     */
    parseThumbnailElements(el) {
        const thumbElements = el.childNodes;
        const items = [];
        let linkEl;
        let size = [];
        let item = {};

        forEach(thumbElements, figureEl => {
            linkEl = figureEl.children[0];
            if (figureEl.nodeType === 1) {
                size = linkEl.getAttribute('data-size').split('x');
                item = {
                    src: linkEl.getAttribute('href'),
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10),
                };
                if (figureEl.children.length > 1) {
                    item['title'] = figureEl.children[1].innerHTML;
                }
                if (linkEl.children.length > 0) {
                    item['msrc'] = linkEl.children[0].getAttribute('src');
                }
                item['el'] = figureEl; // save link to element for getThumbBoundsFn
                items.push(item);
            }
        });
        return items;
    }

    /**
     * Open photoswipe
     *
     * @param index
     * @param galleryElement
     * @param {boolean} disableAnimation
     * @param {boolean} fromURL
     * @returns {{}}
     */
    openPhotoSwipe(index, galleryElement, disableAnimation, fromURL) {
        if (disableAnimation === void 0) {
            disableAnimation = false;
        }
        if (fromURL === void 0) {
            fromURL = false;
        }
        const pswpElement = document.querySelector('.pswp');
        const items = this.parseThumbnailElements(galleryElement);
        const options = {
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),
            getThumbBoundsFn: () => {
                const thumbnail = items[index].el.getElementsByTagName('img')[0];
                const pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                const rect = thumbnail.getBoundingClientRect();
                return {
                    x: rect.left,
                    y: rect.top + pageYScroll,
                    w: rect.width,
                };
            },
        };
        if (fromURL) {
            if (options['galleryPIDs']) {
                for (let j = 0; j < items.length; j += 1) {
                    if (items[j].pid === index) {
                        options['index'] = j;
                        break;
                    }
                }
            } else {
                options['index'] = parseInt(index, 10) - 1;
            }
        } else {
            options['index'] = parseInt(index, 10);
        }
        if (isNaN(options['index'])) {
            return false;
        }
        if (disableAnimation) {
            options['showAnimationDuration'] = 0;
        }
        const gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
        gallery.listen('destroy', () => {
            document.querySelector('.pswp--open').classList.remove('pswp--open');
        });
    }

    /**
     * Opens a thumbnail on click
     *
     * @param e
     * @returns {boolean}
     */
    onThumbnailsClick(e) {
        const closest = (el, fn) => {
            return el && (fn(el) ? el : closest(el.parentNode, fn));
        };
        const eTarget = e.target || e.srcElement;
        const clickedListItem = closest(eTarget, el => {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });
        if (!clickedListItem) {
            return false;
        }
        const clickedGallery = clickedListItem.parentNode;
        const childNodes = clickedListItem.parentNode.childNodes;
        const numChildNodes = childNodes.length;
        let nodeIndex = 0;
        let index;
        for (let i = 0; i < numChildNodes; i += 1) {
            if (childNodes[i].nodeType === 1) {
                if (childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex += 1;
            }
        }
        if (index >= 0) {
            this['Gallery'].openPhotoSwipe(index, clickedGallery, false, false);
        }
        return false;
    }
}
