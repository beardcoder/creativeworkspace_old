export default class ProgressBar {
    constructor() {
        this.progressBar = document.querySelector('.progress-reading-bar');
    }

    static init() {
        const that = new this();
        that.addEventListener();
    }

    addEventListener() {
        for (let _i = 0, _a = ['scroll', 'load']; _i < _a.length; _i++) {
            const event = _a[_i];
            window.addEventListener(event, () => {
                this.progressBar.style.width = `${this.calculatePercentage()}%`;
            }, {passive: true});
        }
    }

    /**
     * @returns {number}
     */
    getDocHeight() {
        return Math.max(
            document.body.scrollHeight,
            document.documentElement.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.offsetHeight,
            document.body.clientHeight,
            document.documentElement.clientHeight
        );
    }

    /**
     * @returns {number}
     */
    calculatePercentage() {
        return ((window.scrollY) / (this.getDocHeight() - window.innerHeight)) * 100;
    }
}
