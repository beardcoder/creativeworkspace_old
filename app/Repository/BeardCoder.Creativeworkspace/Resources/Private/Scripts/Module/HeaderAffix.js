export default class HeaderAffix {
    constructor() {
        this.header = document.getElementById('header');
    }

    static init() {
        const that = new this();
        that.addEvent();
    }

    addEvent() {
        window.addEventListener('scroll', () => {
            if ((window.pageYOffset >= 100)) {
                this.header.classList.add('header--pre-fixed');
            } else {
                this.header.classList.remove('header--pre-fixed');
            }
            if ((window.pageYOffset >= 1000)) {
                this.header.classList.add('header--fixed');
            } else {
                this.header.classList.remove('header--fixed');
            }
        }, {passive: true});
    }
}
