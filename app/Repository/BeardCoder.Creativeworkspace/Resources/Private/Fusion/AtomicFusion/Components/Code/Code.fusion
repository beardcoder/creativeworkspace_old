prototype(BeardCoder.Creativeworkspace:CodeComponent) < prototype(PackageFactory.AtomicFusion:Component) {

    #
    # all fusion properties except renderer are evaluated and passed
    # are made available to the renderer as object ``props`` in the context
    #
    language = ${value + (q(node).property('language') != 'auto' ? 'language-' + q(node).property('language') : '')}
    content = ${q(node).property('content')}

    #
    # the renderer path is evaluated with the props in the context
    # that way regardless off nesting the props can be accessed
    # easily via ${props.__name__}
    #
    renderer = Neos.Fusion:Tag {

        #
        # Name of the tag eg <section></section>
        #
        tagName = 'section'

        #
        # all arguments of the AtomicFusion.classNames eelHelper are evaluated
        # and the following rules are applied
        #
        # - falsy: (null, '', [], {}) -> not rendered
        # - array: all items that are scalar and truthy are rendered as class-name
        # - object: keys that have a truthy values are rendered as class-name
        # - scalar: is cast to string and rendered as class-name
        #
        attributes.class = ${AtomicFusion.classNames('code frame')}

        content = Neos.Fusion:Array {
            headline = Neos.Fusion:Tag {
                tagName = 'div'
                attributes.class = 'frame__text'
                content = Neos.Fusion:Tag {
                    tagName = 'pre'
                    content = Neos.Fusion:Tag {
                        tagName = 'code'
                        attributes.class = ${props.language}
                        content = ${String.htmlSpecialChars(props.content)}
                    }
                }
            }
        }
    }
}

#
# Map node content to a presentational component
#
# instead of Neos.Neos:Content PackageFactory.AtomicFusion:Content
# is used base prototype that adds the needed contentElementWrapping for the backend
#
prototype(BeardCoder.Creativeworkspace:Code) < prototype(PackageFactory.AtomicFusion:Content) {
    renderer = BeardCoder.Creativeworkspace:CodeComponent {
    }
}
