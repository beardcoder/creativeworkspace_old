ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

# TODO: change here
export COMPOSE_PROJECT_NAME=creativeworkspace-neos
export SITE_PACKAGE=BeardCoder.Creativeworkspace
export BASE_VHOST=creativeworkspace.docker

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Create new project
#############################

create:
	bash bin/create-project.sh $(ARGS)

#############################
# Docker machine states
#############################

up:
	docker-compose up -d

start:
	docker-compose start

stop:
	docker-compose stop

state:
	docker-compose ps

restart:
	docker-compose kill $(ARGS)
	docker-compose start $(ARGS)

logs:
	docker-compose logs -f --tail=100 $(ARGS)

.rebuild-container:
	docker-compose rm --force app
	docker-compose build --pull app
	docker-compose up -d

rebuild: .rebuild-container sync

rebuild-gulp:
	docker-compose stop npm
	docker-compose build --no-cache npm
	docker-compose up -d --no-recreate npm

destroy:
	docker-compose down --rmi all

#############################
# MySQL
#############################

wait-mysql:
	bash ./bin/wait-mysql.sh

mysql-backup:
	bash ./bin/backup.sh mysql

mysql-restore:
	bash ./bin/restore.sh mysql

#############################
# General
#############################

bash: shell

shell:
	docker exec -it -u application $$(docker-compose ps -q app) /bin/bash

root:
	docker exec -it -u root $$(docker-compose ps -q app) /bin/bash

restart-gulp:
	docker-compose kill npm
	docker-compose start npm
	docker-compose logs -f --tail=100 npm

sync: composer-install
	docker cp app $$(docker-compose ps -q app):/
	make permissions
	docker exec -ti $$(docker-compose ps -q app) composer install --no-progress --no-interaction
	docker exec -u application $$(docker-compose ps -q app) ./flow flow:package:rescan

npm-shell:
	docker exec -it -u node $$(docker-compose ps -q npm) bash

#############################
# Utilities
#############################

composer:
	docker run --rm -ti -v $$(pwd)/app:/app composer:1 $(ARGS)

composer-install:
	docker run --rm -ti -v $$(pwd)/app:/app composer:1 install --no-interaction

from-ground-up: rebuild setup urls

#############################
# Linting
#############################

lint: lint-sass lint-js lint-xlf lint-php

lint-sass:
	docker exec -it -u node $$(docker-compose ps -q npm) yarn run gulp styles:lint

lint-js:
	docker exec -it -u node $$(docker-compose ps -q npm) yarn run gulp eslint

lint-xlf:
	docker run --rm -ti -v $$(pwd)/app:/app 1drop/php-70-docker-utils /bin/sh -c "find /app/Repository -type f -name "*.xlf" -exec /root/.composer/vendor/bin/xml-linter {} \;"

lint-php:
	docker run --rm -ti -v $$(pwd):/app -w /app 1drop/php-70-docker-utils /bin/sh -c "php-cs-fixer fix -v --dry-run --using-cache=no"

#############################
# Flow
#############################

unit-tests:
	docker exec $$(docker-compose ps -q app) ./bin/phpunit -c Build/BuildEssentials/PhpUnit/UnitTests.xml Repository

flow:
	docker exec -ti $$(docker-compose ps -q app) ./flow $(ARGS)

setup: permissions
	docker exec -u application $$(docker-compose ps -q app) composer install
	docker exec -u application $$(docker-compose ps -q app) ./flow doctrine:migrate
	docker exec -u application $$(docker-compose ps -q app) ./flow flow:package:rescan
	-docker exec -u application $$(docker-compose ps -q app) ./flow site:prune --site-node='*'
	docker exec -u application $$(docker-compose ps -q app) ./flow media:removeunused --assume-yes
	docker exec -u application $$(docker-compose ps -q app) ./flow site:import --package-key=$$SITE_PACKAGE
	-docker exec -u application -it $$(docker-compose ps -q app) ./flow user:create --roles="Neos.Neos:Administrator" $$(whoami) q1w2e3r4 One Drop
	docker exec -u application $$(docker-compose ps -q app) ./flow flow:cache:flush --force
	docker exec -u application $$(docker-compose ps -q app) ./flow resource:publish

export-site:
	docker exec -u application $$(docker-compose ps -q app) ./flow site:export --package-key=$$SITE_PACKAGE --tidy

import-site:
	-docker exec -u application $$(docker-compose ps -q app) ./flow site:prune --site-node='*'
	docker exec -u application $$(docker-compose ps -q app) ./flow site:import --package-key=$$SITE_PACKAGE

#############################
# Helper
#############################

permissions:
	docker exec -ti $$(docker-compose ps -q app) chown -R 1000:1000 /app &>/dev/null | true
	docker exec -ti $$(docker-compose ps -q app) chown -R 1000:1000 /opt/tmp &>/dev/null | true

urls:
	echo "---------------------------------------------------"
	echo "You can access your project at the following URLS:"
	echo "---------------------------------------------------"
	echo ""
	echo "Backend:     http://${BASE_VHOST}/neos/"
	echo "Username:    $$(whoami)"
	echo "Password:    q1w2e3r4"
	echo ""
	echo "Frontend:    http://${BASE_VHOST}/"
	echo "Browsersync: http://sync.${BASE_VHOST}/"
	echo "Mailhog:     http://mail.${BASE_VHOST}/"
	echo ""

#############################
# Argument fix workaround
#############################
%:
	@:
