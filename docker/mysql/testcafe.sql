# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: db.docker (MySQL 5.5.5-10.2.16-MariaDB-1:10.2.16+maria~bionic-log)
# Datenbank: neos
# Erstellt am: 2018-07-30 09:23:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Export von Tabelle flow_doctrine_migrationstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flow_doctrine_migrationstatus`;

CREATE TABLE `flow_doctrine_migrationstatus` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `flow_doctrine_migrationstatus` WRITE;
/*!40000 ALTER TABLE `flow_doctrine_migrationstatus` DISABLE KEYS */;

INSERT INTO `flow_doctrine_migrationstatus` (`version`)
VALUES
	('20110613223837'),
	('20110613224537'),
	('20110620155001'),
	('20110620155002'),
	('20110714212900'),
	('20110824124835'),
	('20110824124935'),
	('20110824125035'),
	('20110824125135'),
	('20110919164835'),
	('20110920104739'),
	('20110920125736'),
	('20110923125535'),
	('20110923125536'),
	('20110923125537'),
	('20110923125538'),
	('20110925123119'),
	('20110925123120'),
	('20110928114048'),
	('20111215172027'),
	('20120328152041'),
	('20120329220340'),
	('20120329220341'),
	('20120329220342'),
	('20120329220343'),
	('20120329220344'),
	('20120412093748'),
	('20120429213445'),
	('20120429213446'),
	('20120429213447'),
	('20120429213448'),
	('20120520211354'),
	('20120521125401'),
	('20120525141545'),
	('20120625211647'),
	('20120829124549'),
	('20120930203452'),
	('20120930211542'),
	('20121001181137'),
	('20121001201712'),
	('20121001202223'),
	('20121011140946'),
	('20121014005902'),
	('20121030221151'),
	('20121031190213'),
	('20130201104344'),
	('20130213130515'),
	('20130218100324'),
	('20130319131400'),
	('20130522131641'),
	('20130522132835'),
	('20130605174712'),
	('20130702151425'),
	('20130730151319'),
	('20130919143352'),
	('20130930182839'),
	('20131111235827'),
	('20131129110302'),
	('20131205174631'),
	('20140206124123'),
	('20140208173140'),
	('20140325173151'),
	('20140826164246'),
	('20141001151417'),
	('20141003233738'),
	('20141015125841'),
	('20141017174559'),
	('20141113173712'),
	('20141114115241'),
	('20141118172322'),
	('20150129225152'),
	('20150131174806'),
	('20150206113911'),
	('20150206114820'),
	('20150211181736'),
	('20150217145853'),
	('20150224171107'),
	('20150228154201'),
	('20150309181635'),
	('20150309181636'),
	('20150309215316'),
	('20150309215317'),
	('20150324185018'),
	('20150324185019'),
	('20150408112832'),
	('20150507204450'),
	('20150507204452'),
	('20150524150234'),
	('20150611154419'),
	('20150612093351'),
	('20150623112200'),
	('20150701113246'),
	('20150724091148'),
	('20150913173832'),
	('20151110113650'),
	('20151117125437'),
	('20151117125551'),
	('20151216052338'),
	('20151216143756'),
	('20151216144408'),
	('20151223125909'),
	('20160104121311'),
	('20160212141523'),
	('20160212141524'),
	('20160223165602'),
	('20160223165603'),
	('20160223165604'),
	('20160411101457'),
	('20160411101458'),
	('20160418100005'),
	('20160601164332'),
	('20160711103441'),
	('20161115142022'),
	('20161124185047'),
	('20161124230946'),
	('20161125093800'),
	('20161125093810'),
	('20161125093820'),
	('20161125121218'),
	('20161125123504'),
	('20161125124749'),
	('20161125171231'),
	('20161125172223'),
	('20161125175046'),
	('20170110130113'),
	('20170110130149'),
	('20170110130217'),
	('20170110130253'),
	('20170110130325'),
	('20170115114801'),
	('20170220155800'),
	('20170328183556'),
	('20171206140453'),
	('20180405104603'),
	('20180622074421');

/*!40000 ALTER TABLE `flow_doctrine_migrationstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle neos_contentrepository_domain_model_contentobjectproxy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_contentrepository_domain_model_contentobjectproxy`;

CREATE TABLE `neos_contentrepository_domain_model_contentobjectproxy` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `targettype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `targetid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_contentrepository_domain_model_nodedata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_contentrepository_domain_model_nodedata`;

CREATE TABLE `neos_contentrepository_domain_model_nodedata` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `workspace` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contentobjectproxy` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(4000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sortingindex` int(11) DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:flow_json_array)',
  `nodetype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `removed` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `hiddenbeforedatetime` datetime DEFAULT NULL,
  `hiddenafterdatetime` datetime DEFAULT NULL,
  `hiddeninindex` tinyint(1) NOT NULL,
  `accessroles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:flow_json_array)',
  `version` int(11) NOT NULL DEFAULT 1,
  `parentpath` varchar(4000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pathhash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dimensionshash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dimensionvalues` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:flow_json_array)',
  `parentpathhash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movedto` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creationdatetime` datetime NOT NULL,
  `lastmodificationdatetime` datetime NOT NULL,
  `lastpublicationdatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_CE6515692DBEC7578D94001992F8FB01` (`pathhash`,`workspace`,`dimensionshash`),
  UNIQUE KEY `UNIQ_CE651569772E836A8D94001992F8FB012D45FE4D` (`identifier`,`workspace`,`dimensionshash`,`movedto`),
  KEY `parentpath_sortingindex` (`parentpathhash`,`sortingindex`),
  KEY `identifierindex` (`identifier`),
  KEY `nodetypeindex` (`nodetype`),
  KEY `IDX_CE6515698D940019` (`workspace`),
  KEY `IDX_CE6515694930C33C` (`contentobjectproxy`),
  KEY `IDX_CE6515692D45FE4D` (`movedto`),
  KEY `parentpath` (`parentpath`(255)),
  CONSTRAINT `FK_60A956B92D45FE4D` FOREIGN KEY (`movedto`) REFERENCES `neos_contentrepository_domain_model_nodedata` (`persistence_object_identifier`) ON DELETE SET NULL,
  CONSTRAINT `FK_60A956B98D940019` FOREIGN KEY (`workspace`) REFERENCES `neos_contentrepository_domain_model_workspace` (`name`) ON DELETE SET NULL,
  CONSTRAINT `neos_contentrepository_domain_model_nodedata_ibfk_2` FOREIGN KEY (`contentobjectproxy`) REFERENCES `neos_contentrepository_domain_model_contentobjectproxy` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_contentrepository_domain_model_nodedimension
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_contentrepository_domain_model_nodedimension`;

CREATE TABLE `neos_contentrepository_domain_model_nodedimension` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nodedata` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_C4713BFF93BDC8E25E237E061D775834` (`nodedata`,`name`,`value`),
  KEY `IDX_C4713BFF93BDC8E2` (`nodedata`),
  CONSTRAINT `FK_6C144D3693BDC8E2` FOREIGN KEY (`nodedata`) REFERENCES `neos_contentrepository_domain_model_nodedata` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_contentrepository_domain_model_workspace
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_contentrepository_domain_model_workspace`;

CREATE TABLE `neos_contentrepository_domain_model_workspace` (
  `baseworkspace` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rootnodedata` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `IDX_F7A3826CE9BFE681` (`baseworkspace`),
  KEY `IDX_F7A3826CBB46155` (`rootnodedata`),
  CONSTRAINT `FK_71DE9CFBBB46155` FOREIGN KEY (`rootnodedata`) REFERENCES `neos_contentrepository_domain_model_nodedata` (`persistence_object_identifier`),
  CONSTRAINT `FK_71DE9CFBE9BFE681` FOREIGN KEY (`baseworkspace`) REFERENCES `neos_contentrepository_domain_model_workspace` (`name`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_contentrepository_migration_domain_model_migrationstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_contentrepository_migration_domain_model_migrationstatus`;

CREATE TABLE `neos_contentrepository_migration_domain_model_migrationstatus` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationtimestamp` datetime NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_flow_mvc_routing_objectpathmapping
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_flow_mvc_routing_objectpathmapping`;

CREATE TABLE `neos_flow_mvc_routing_objectpathmapping` (
  `objecttype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uripattern` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pathsegment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`objecttype`,`uripattern`,`pathsegment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_flow_resourcemanagement_persistentresource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_flow_resourcemanagement_persistentresource`;

CREATE TABLE `neos_flow_resourcemanagement_persistentresource` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sha1` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `md5` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collectionname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mediatype` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relativepublicationpath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filesize` decimal(20,0) NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_35DC14F03332102A` (`sha1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_flow_security_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_flow_security_account`;

CREATE TABLE `neos_flow_security_account` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountidentifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authenticationprovidername` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credentialssource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `expirationdate` datetime DEFAULT NULL,
  `roleidentifiers` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:simple_array)',
  `lastsuccessfulauthenticationdate` datetime DEFAULT NULL,
  `failedauthenticationcount` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `flow_identity_neos_flow_security_account` (`accountidentifier`,`authenticationprovidername`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_adjustment_abstractimageadjustment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_adjustment_abstractimageadjustment`;

CREATE TABLE `neos_media_domain_model_adjustment_abstractimageadjustment` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagevariant` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `maximumwidth` int(11) DEFAULT NULL,
  `maximumheight` int(11) DEFAULT NULL,
  `minimumwidth` int(11) DEFAULT NULL,
  `minimumheight` int(11) DEFAULT NULL,
  `ratiomode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `allowupscaling` tinyint(1) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_8B2F26F8A76D06E6` (`imagevariant`),
  CONSTRAINT `FK_84416FDCA76D06E6` FOREIGN KEY (`imagevariant`) REFERENCES `neos_media_domain_model_imagevariant` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_asset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_asset`;

CREATE TABLE `neos_media_domain_model_asset` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastmodified` datetime NOT NULL,
  `assetsourceidentifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'neos',
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_675F9550BC91F416` (`resource`),
  CONSTRAINT `FK_B8306B8EBC91F416` FOREIGN KEY (`resource`) REFERENCES `neos_flow_resourcemanagement_persistentresource` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_asset_tags_join
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_asset_tags_join`;

CREATE TABLE `neos_media_domain_model_asset_tags_join` (
  `media_asset` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_tag` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`media_asset`,`media_tag`),
  KEY `IDX_915BC7A21DB69EED` (`media_asset`),
  KEY `IDX_915BC7A248D8C57E` (`media_tag`),
  CONSTRAINT `FK_DAF7A1EB1DB69EED` FOREIGN KEY (`media_asset`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`),
  CONSTRAINT `FK_DAF7A1EB48D8C57E` FOREIGN KEY (`media_tag`) REFERENCES `neos_media_domain_model_tag` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_assetcollection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_assetcollection`;

CREATE TABLE `neos_media_domain_model_assetcollection` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_assetcollection_assets_join
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_assetcollection_assets_join`;

CREATE TABLE `neos_media_domain_model_assetcollection_assets_join` (
  `media_assetcollection` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_asset` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`media_assetcollection`,`media_asset`),
  KEY `IDX_1305D4CE2A965871` (`media_assetcollection`),
  KEY `IDX_1305D4CE1DB69EED` (`media_asset`),
  CONSTRAINT `FK_E90D72511DB69EED` FOREIGN KEY (`media_asset`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`),
  CONSTRAINT `FK_E90D72512A965871` FOREIGN KEY (`media_assetcollection`) REFERENCES `neos_media_domain_model_assetcollection` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_assetcollection_tags_join
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_assetcollection_tags_join`;

CREATE TABLE `neos_media_domain_model_assetcollection_tags_join` (
  `media_assetcollection` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_tag` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`media_assetcollection`,`media_tag`),
  KEY `IDX_522F02632A965871` (`media_assetcollection`),
  KEY `IDX_522F026348D8C57E` (`media_tag`),
  CONSTRAINT `FK_A41705672A965871` FOREIGN KEY (`media_assetcollection`) REFERENCES `neos_media_domain_model_assetcollection` (`persistence_object_identifier`),
  CONSTRAINT `FK_A417056748D8C57E` FOREIGN KEY (`media_tag`) REFERENCES `neos_media_domain_model_tag` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_audio
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_audio`;

CREATE TABLE `neos_media_domain_model_audio` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  CONSTRAINT `FK_A2E2074747A46B0A` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_document`;

CREATE TABLE `neos_media_domain_model_document` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  CONSTRAINT `FK_F089E2F547A46B0A` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_image`;

CREATE TABLE `neos_media_domain_model_image` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  CONSTRAINT `FK_7FA2358D47A46B0A` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_imagevariant
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_imagevariant`;

CREATE TABLE `neos_media_domain_model_imagevariant` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `originalasset` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_C4BF979F55FF4171` (`originalasset`),
  CONSTRAINT `FK_758EDEBD47A46B0A` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`) ON DELETE CASCADE,
  CONSTRAINT `FK_758EDEBD55FF4171` FOREIGN KEY (`originalasset`) REFERENCES `neos_media_domain_model_image` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_importedasset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_importedasset`;

CREATE TABLE `neos_media_domain_model_importedasset` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assetsourceidentifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remoteassetidentifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localassetidentifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localoriginalassetidentifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `importedat` datetime NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `flow_identity_neos_media_domain_model_importedasset` (`assetsourceidentifier`,`remoteassetidentifier`,`localassetidentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_tag`;

CREATE TABLE `neos_media_domain_model_tag` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_thumbnail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_thumbnail`;

CREATE TABLE `neos_media_domain_model_thumbnail` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `originalasset` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `configuration` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:flow_json_array)',
  `configurationhash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staticresource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_3A163C4955FF41717F7CBA1A` (`originalasset`,`configurationhash`),
  UNIQUE KEY `UNIQ_3A163C49BC91F416` (`resource`),
  KEY `IDX_3A163C4955FF4171` (`originalasset`),
  CONSTRAINT `FK_B7CE141455FF4171` FOREIGN KEY (`originalasset`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`),
  CONSTRAINT `FK_B7CE1414BC91F416` FOREIGN KEY (`resource`) REFERENCES `neos_flow_resourcemanagement_persistentresource` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_media_domain_model_video
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_media_domain_model_video`;

CREATE TABLE `neos_media_domain_model_video` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  CONSTRAINT `FK_C658EBFE47A46B0A` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_media_domain_model_asset` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_neos_domain_model_domain
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_neos_domain_model_domain`;

CREATE TABLE `neos_neos_domain_model_domain` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `scheme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `flow_identity_neos_neos_domain_model_domain` (`hostname`),
  KEY `IDX_51265BE9694309E4` (`site`),
  CONSTRAINT `neos_neos_domain_model_domain_ibfk_1` FOREIGN KEY (`site`) REFERENCES `neos_neos_domain_model_site` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_neos_domain_model_site
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_neos_domain_model_site`;

CREATE TABLE `neos_neos_domain_model_site` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nodename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `siteresourcespackagekey` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assetcollection` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primarydomain` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `flow_identity_neos_neos_domain_model_site` (`nodename`),
  KEY `IDX_9B02A4EB8872B4A` (`primarydomain`),
  KEY `IDX_9B02A4E5CEB2C15` (`assetcollection`),
  CONSTRAINT `FK_1854B2075CEB2C15` FOREIGN KEY (`assetcollection`) REFERENCES `neos_media_domain_model_assetcollection` (`persistence_object_identifier`),
  CONSTRAINT `FK_1854B207B8872B4A` FOREIGN KEY (`primarydomain`) REFERENCES `neos_neos_domain_model_domain` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_neos_domain_model_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_neos_domain_model_user`;

CREATE TABLE `neos_neos_domain_model_user` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preferences` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_ED60F5E3E931A6F5` (`preferences`),
  CONSTRAINT `neos_neos_domain_model_user_ibfk_1` FOREIGN KEY (`preferences`) REFERENCES `neos_neos_domain_model_userpreferences` (`persistence_object_identifier`),
  CONSTRAINT `neos_neos_domain_model_user_ibfk_2` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_party_domain_model_abstractparty` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_neos_domain_model_userpreferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_neos_domain_model_userpreferences`;

CREATE TABLE `neos_neos_domain_model_userpreferences` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preferences` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_neos_eventlog_domain_model_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_neos_eventlog_domain_model_event`;

CREATE TABLE `neos_neos_eventlog_domain_model_event` (
  `parentevent` int(10) unsigned DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountidentifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:flow_json_array)',
  `dtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nodeidentifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documentnodeidentifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workspacename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dimension` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`uid`),
  KEY `eventtype` (`eventtype`),
  KEY `IDX_D6DBC30A5B684C08` (`parentevent`),
  KEY `documentnodeidentifier` (`documentnodeidentifier`),
  CONSTRAINT `FK_30AB3A75B684C08` FOREIGN KEY (`parentevent`) REFERENCES `neos_neos_eventlog_domain_model_event` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_party_domain_model_abstractparty
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_party_domain_model_abstractparty`;

CREATE TABLE `neos_party_domain_model_abstractparty` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_party_domain_model_abstractparty_accounts_join
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_party_domain_model_abstractparty_accounts_join`;

CREATE TABLE `neos_party_domain_model_abstractparty_accounts_join` (
  `party_abstractparty` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flow_security_account` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`party_abstractparty`,`flow_security_account`),
  UNIQUE KEY `UNIQ_E4E61AB058842EFC` (`flow_security_account`),
  KEY `IDX_E4E61AB038110E12` (`party_abstractparty`),
  CONSTRAINT `FK_1EEEBC2F38110E12` FOREIGN KEY (`party_abstractparty`) REFERENCES `neos_party_domain_model_abstractparty` (`persistence_object_identifier`),
  CONSTRAINT `FK_1EEEBC2F58842EFC` FOREIGN KEY (`flow_security_account`) REFERENCES `neos_flow_security_account` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_party_domain_model_electronicaddress
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_party_domain_model_electronicaddress`;

CREATE TABLE `neos_party_domain_model_electronicaddress` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usagetype` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` tinyint(1) NOT NULL,
  `dtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_party_domain_model_person
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_party_domain_model_person`;

CREATE TABLE `neos_party_domain_model_person` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryelectronicaddress` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_A7B0E9CC5E237E06` (`name`),
  KEY `IDX_A7B0E9CCA7CECF13` (`primaryelectronicaddress`),
  CONSTRAINT `neos_party_domain_model_person_ibfk_1` FOREIGN KEY (`name`) REFERENCES `neos_party_domain_model_personname` (`persistence_object_identifier`),
  CONSTRAINT `neos_party_domain_model_person_ibfk_2` FOREIGN KEY (`primaryelectronicaddress`) REFERENCES `neos_party_domain_model_electronicaddress` (`persistence_object_identifier`),
  CONSTRAINT `neos_party_domain_model_person_ibfk_3` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `neos_party_domain_model_abstractparty` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_party_domain_model_person_electronicaddresses_join
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_party_domain_model_person_electronicaddresses_join`;

CREATE TABLE `neos_party_domain_model_person_electronicaddresses_join` (
  `party_person` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `party_electronicaddress` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`party_person`,`party_electronicaddress`),
  KEY `IDX_131A08DD72AAAA2F` (`party_person`),
  KEY `IDX_131A08DDB06BD60D` (`party_electronicaddress`),
  CONSTRAINT `neos_party_domain_model_person_electronicaddresses_join_ibfk_1` FOREIGN KEY (`party_person`) REFERENCES `neos_party_domain_model_person` (`persistence_object_identifier`),
  CONSTRAINT `neos_party_domain_model_person_electronicaddresses_join_ibfk_2` FOREIGN KEY (`party_electronicaddress`) REFERENCES `neos_party_domain_model_electronicaddress` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_party_domain_model_personname
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_party_domain_model_personname`;

CREATE TABLE `neos_party_domain_model_personname` (
  `persistence_object_identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `othername` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle neos_redirecthandler_databasestorage_domain_model_redirect
# ------------------------------------------------------------

DROP TABLE IF EXISTS `neos_redirecthandler_databasestorage_domain_model_redirect`;

CREATE TABLE `neos_redirecthandler_databasestorage_domain_model_redirect` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `creationdatetime` datetime NOT NULL,
  `lastmodificationdatetime` datetime NOT NULL,
  `version` int(11) NOT NULL DEFAULT 1,
  `sourceuripath` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `sourceuripathhash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `targeturipath` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `targeturipathhash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `statuscode` int(11) NOT NULL,
  `host` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hitcounter` int(11) NOT NULL,
  `lasthit` datetime DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `flow_identity_neos_redirecthandler_databasestorage_domain_60892` (`sourceuripathhash`,`host`),
  KEY `sourceuripathhash` (`sourceuripathhash`,`host`),
  KEY `targeturipathhash` (`targeturipathhash`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
